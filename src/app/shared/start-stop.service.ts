import { Injectable } from '@angular/core';
import { MessageService } from './message.service';

@Injectable()

export class StartStopService {
  constructor(private messageService: MessageService) {}

  start() {
    this.messageService.fetchMessages();
  }

  stop() {
    return console.log('привет');
  }
}
