import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Message } from './message.model';
import { map, Subject, tap } from 'rxjs';

@Injectable()

export class MessageService {
  messagesChange = new Subject<Message[]>();
  messagesFetching = new Subject<boolean>();
  messageUpLoading = new Subject<boolean>();

  private messages: Message[] = [];

  constructor(private http: HttpClient) {}

  getDate(str: string) {
    let date = new Date(str);
    return date.toLocaleString();
  }

  fetchMessages() {
    this.messagesFetching.next(true);
    this.http.get<{[id: string]: Message}>('http://146.185.154.90:8000/messages')
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const messageData = result[id];

          return new Message(
            id,
            messageData.author,
            this.getDate(messageData.datetime),
            messageData.message,
          );
        });
      }))
      .subscribe(messages => {
        this.messages = messages;
        this.messagesChange.next(this.messages.slice());
        this.messagesFetching.next(false);
      });
  }

  addMessage(author: string, message: string) {
    const body = new HttpParams()
    .set('author', author)
    .set('message', message);

    this.messageUpLoading.next(true);

    return this.http.post('http://146.185.154.90:8000/messages', body).pipe(
      tap(() => {
        this.messageUpLoading.next(false);
      }, () => {
        this.messageUpLoading.next(false);
      })
    );
  }
}
