export class Message {
  constructor(
    public id: string,
    public author: string,
    public datetime: any,
    public message: string,
  ) {}
}
