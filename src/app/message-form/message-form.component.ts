import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MessageService } from '../shared/message.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-message-form',
  templateUrl: './message-form.component.html',
  styleUrls: ['./message-form.component.css']
})
export class MessageFormComponent implements OnInit, OnDestroy {
  @ViewChild('form') messageForm!: NgForm;

  isUpLoading = false;
  messageUploadingSubscription!: Subscription;

  constructor(private messageService: MessageService) { }

  ngOnInit(): void {
    this.messageUploadingSubscription = this.messageService.messageUpLoading.subscribe((isUpLoading: boolean) => {
      this.isUpLoading = isUpLoading;
    });
  }

  sendMessage() {
    this.messageService.addMessage(this.messageForm.value.author, this.messageForm.value.message).subscribe(() => {
      this.messageService.fetchMessages();
    });
  }

  ngOnDestroy() {
    this.messageUploadingSubscription.unsubscribe();
  }

}
