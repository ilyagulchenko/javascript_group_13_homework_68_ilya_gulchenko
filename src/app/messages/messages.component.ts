import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MessageService } from '../shared/message.service';
import { Message } from '../shared/message.model';
import { Subscription } from 'rxjs';
import { StartStopService } from '../shared/start-stop.service';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit, OnDestroy {
  messages!: Message[];
  messagesChangeSubscription!: Subscription;
  messagesFetchingSubscriptions!: Subscription;
  isFetching: boolean = false;

  constructor(
    private http: HttpClient,
    private messageService: MessageService,
    private startStopService: StartStopService
  ) { }

  ngOnInit(): void {
    this.messagesChangeSubscription = this.messageService.messagesChange.subscribe((messages: Message[]) => {
      this.messages = messages;
    });
    this.messagesFetchingSubscriptions = this.messageService.messagesFetching.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });
    this.startStopService.start();
  }

  ngOnDestroy(): void {
    this.startStopService.stop();
    this.messagesChangeSubscription.unsubscribe();
    this.messagesFetchingSubscriptions.unsubscribe();
  }
}
