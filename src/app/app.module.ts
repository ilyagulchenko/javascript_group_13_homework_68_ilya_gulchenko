import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { MessageFormComponent } from './message-form/message-form.component';
import { MessageItemComponent } from './message-item/message-item.component';
import { MessagesComponent } from './messages/messages.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MessageService } from './shared/message.service';
import { StartStopService } from './shared/start-stop.service';

@NgModule({
  declarations: [
    AppComponent,
    MessageFormComponent,
    MessageItemComponent,
    MessagesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [MessageService, StartStopService],
  bootstrap: [AppComponent]
})
export class AppModule { }
